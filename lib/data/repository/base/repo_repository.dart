


import 'package:github_repo_search/core/resources/data_state.dart';
import 'package:github_repo_search/data/models/base/repo.dart';
import 'package:github_repo_search/data/models/repo.dart';
import 'package:github_repo_search/data/models/repo_response.dart';

abstract class RepoRepository {
  // API methods
  Future<DataState<RepoResponse>> getGithubRepo(int pageNo);

  // Database methods
  Future < List < RepoModel >> getSavedRepo();

  Future < void > saveRepo(RepoEntity repoEntity);

  // Future < void > removeArticle(RepoEntity repoEntity);
}