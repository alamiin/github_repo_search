

import 'package:github_repo_search/core/resources/data_state.dart';
import 'package:github_repo_search/core/usecase/usecase.dart';
import 'package:github_repo_search/data/models/base/repo.dart';
import 'package:github_repo_search/data/models/repo.dart';
import 'package:github_repo_search/data/models/repo_response.dart';
import 'package:github_repo_search/data/repository/base/repo_repository.dart';

class GetSavedRepoUseCase implements UseCase<List<RepoEntity>,void>{
  
  final RepoRepository _repoRepository;

  GetSavedRepoUseCase(this._repoRepository);
  
  @override
  Future<List<RepoModel>> call({ void params }) {
    return _repoRepository.getSavedRepo();
  }

  
}